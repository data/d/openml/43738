# OpenML dataset: Masters-Degrees--Programs-(mastersportal.eu)

https://www.openml.org/d/43738

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I was searching for a master degree program in data-science when I found this awesome website mastersportal, So I just scrapped it to take my time analysing all master programs available around the world.
Content
This dataset contains 60442 different master's degree programs from 99 countries around the world.
Scrapping code
https://github.com/AnasFullStack/Masters-Portal-Scrapper

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43738) of an [OpenML dataset](https://www.openml.org/d/43738). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43738/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43738/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43738/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

